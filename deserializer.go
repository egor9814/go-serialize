package go_serialize

import (
	"bytes"
	"errors"
	"io"
	"math"
	"strconv"
)

type Deserializer struct {
	in io.Reader
}

func NewDeserializer(in io.Reader) *Deserializer {
	return &Deserializer{in: in}
}

func NewByteArrayDeserializer(data []byte) *Deserializer {
	return NewDeserializer(bytes.NewReader(data))
}

func (d *Deserializer) Bytes(out []byte) error {
	if n, err := d.in.Read(out); err != nil {
		return err
	} else if n != len(out) {
		return errors.New("cannot read all " + strconv.Itoa(n) + " bytes")
	} else {
		return nil
	}
}

func (d *Deserializer) MakeBytes(size int) ([]byte, error) {
	data := make([]byte, size)
	if err := d.Bytes(data); err != nil {
		return nil, err
	}
	return data, nil
}

func (d *Deserializer) UInt8() (uint8, error) {
	data := []byte{0}
	if err := d.Bytes(data); err != nil {
		return 0, err
	}
	return data[0], nil
}

func (d *Deserializer) Byte() (byte, error) {
	return d.UInt8()
}

func (d *Deserializer) Int8() (int8, error) {
	if ui, err := d.UInt8(); err != nil {
		return 0, err
	} else {
		return int8(ui), nil
	}
}

func (d *Deserializer) UInt16() (uint16, error) {
	data := []byte{0, 0}
	if err := d.Bytes(data); err != nil {
		return 0, err
	}
	return (uint16(data[0]) << 8) | (uint16(data[1])), nil
}

func (d *Deserializer) Int16() (int16, error) {
	if ui, err := d.UInt16(); err != nil {
		return 0, err
	} else {
		return int16(ui), nil
	}
}

func (d *Deserializer) UInt32() (uint32, error) {
	data := []byte{0, 0, 0, 0}
	if err := d.Bytes(data); err != nil {
		return 0, err
	}
	return (uint32(data[0]) << 24) | (uint32(data[1]) << 16) |
		(uint32(data[2]) << 8) | (uint32(data[3])), nil
}

func (d *Deserializer) Int32() (int32, error) {
	if ui, err := d.UInt32(); err != nil {
		return 0, err
	} else {
		return int32(ui), nil
	}
}

func (d *Deserializer) UInt() (uint, error) {
	if ui, err := d.UInt32(); err != nil {
		return 0, err
	} else {
		return uint(ui), nil
	}
}

func (d *Deserializer) Int() (int, error) {
	if i, err := d.Int32(); err != nil {
		return 0, err
	} else {
		return int(i), nil
	}
}

func (d *Deserializer) UInt64() (uint64, error) {
	data := []byte{0, 0, 0, 0, 0, 0, 0, 0}
	if err := d.Bytes(data); err != nil {
		return 0, err
	}
	return (uint64(data[0]) << 56) | (uint64(data[1]) << 48) |
		(uint64(data[2]) << 40) | (uint64(data[3]) << 32) |
		(uint64(data[4]) << 24) | (uint64(data[5]) << 16) |
		(uint64(data[6]) << 8) | (uint64(data[7])), nil
}

func (d *Deserializer) Int64() (int64, error) {
	if ui, err := d.UInt64(); err != nil {
		return 0, err
	} else {
		return int64(ui), nil
	}
}

func (d *Deserializer) Float32() (float32, error) {
	if ui, err := d.UInt32(); err != nil {
		return 0, err
	} else {
		return math.Float32frombits(ui), nil
	}
}

func (d *Deserializer) Float64() (float64, error) {
	if ui, err := d.UInt64(); err != nil {
		return 0, err
	} else {
		return math.Float64frombits(ui), nil
	}
}

func (d *Deserializer) Bool() (bool, error) {
	if b, err := d.Byte(); err != nil {
		return false, err
	} else {
		return b != 0, nil
	}
}

func (d *Deserializer) String() (string, error) {
	l, err := d.Int()
	if err != nil {
		return "", err
	}
	data := make([]byte, l)
	if err = d.Bytes(data); err != nil {
		return "", err
	}
	return string(data), nil
}

func (d *Deserializer) Serializable(value ISerializable) error {
	return value.Deserialize(d)
}

func (d *Deserializer) UInt8Ptr(out *uint8) error {
	if ui, err := d.UInt8(); err != nil {
		return err
	} else {
		*out = ui
		return nil
	}
}

func (d *Deserializer) BytePtr(out *byte) error {
	if ui, err := d.Byte(); err != nil {
		return err
	} else {
		*out = ui
		return nil
	}
}

func (d *Deserializer) Int8Ptr(out *int8) error {
	if i, err := d.Int8(); err != nil {
		return err
	} else {
		*out = i
		return nil
	}
}

func (d *Deserializer) UInt16Ptr(out *uint16) error {
	if ui, err := d.UInt16(); err != nil {
		return err
	} else {
		*out = ui
		return nil
	}
}

func (d *Deserializer) Int16Ptr(out *int16) error {
	if i, err := d.Int16(); err != nil {
		return err
	} else {
		*out = i
		return nil
	}
}

func (d *Deserializer) UInt32Ptr(out *uint32) error {
	if ui, err := d.UInt32(); err != nil {
		return err
	} else {
		*out = ui
		return nil
	}
}

func (d *Deserializer) Int32Ptr(out *int32) error {
	if i, err := d.Int32(); err != nil {
		return err
	} else {
		*out = i
		return nil
	}
}

func (d *Deserializer) UIntPtr(out *uint) error {
	if ui, err := d.UInt(); err != nil {
		return err
	} else {
		*out = ui
		return nil
	}
}

func (d *Deserializer) IntPtr(out *int) error {
	if i, err := d.Int(); err != nil {
		return err
	} else {
		*out = i
		return nil
	}
}

func (d *Deserializer) UInt64Ptr(out *uint64) error {
	if ui, err := d.UInt64(); err != nil {
		return err
	} else {
		*out = ui
		return nil
	}
}

func (d *Deserializer) Int64Ptr(out *int64) error {
	if i, err := d.Int64(); err != nil {
		return err
	} else {
		*out = i
		return nil
	}
}

func (d *Deserializer) Float32Ptr(out *float32) error {
	if f, err := d.Float32(); err != nil {
		return err
	} else {
		*out = f
		return nil
	}
}

func (d *Deserializer) Float64Ptr(out *float64) error {
	if f, err := d.Float64(); err != nil {
		return err
	} else {
		*out = f
		return nil
	}
}

func (d *Deserializer) BoolPtr(out *bool) error {
	if b, err := d.Bool(); err != nil {
		return err
	} else {
		*out = b
		return nil
	}
}

func (d *Deserializer) StringPtr(out *string) error {
	if s, err := d.String(); err != nil {
		return err
	} else {
		*out = s
		return nil
	}
}

func (d *Deserializer) MustBytes(out []byte) {
	if err := d.Bytes(out); err != nil {
		panic(err)
	}
}

func (d *Deserializer) MustMakeBytes(size int) []byte {
	if val, err := d.MakeBytes(size); err != nil {
		panic(err)
	} else {
		return val
	}
}

func (d *Deserializer) MustUInt8() uint8 {
	if val, err := d.UInt8(); err != nil {
		panic(err)
	} else {
		return val
	}
}

func (d *Deserializer) MustByte() byte {
	if val, err := d.Byte(); err != nil {
		panic(err)
	} else {
		return val
	}
}

func (d *Deserializer) MustInt8() int8 {
	if val, err := d.Int8(); err != nil {
		panic(err)
	} else {
		return val
	}
}

func (d *Deserializer) MustUInt16() uint16 {
	if val, err := d.UInt16(); err != nil {
		panic(err)
	} else {
		return val
	}
}

func (d *Deserializer) MustInt16() int16 {
	if val, err := d.Int16(); err != nil {
		panic(err)
	} else {
		return val
	}
}

func (d *Deserializer) MustUInt32() uint32 {
	if val, err := d.UInt32(); err != nil {
		panic(err)
	} else {
		return val
	}
}

func (d *Deserializer) MustInt32() int32 {
	if val, err := d.Int32(); err != nil {
		panic(err)
	} else {
		return val
	}
}

func (d *Deserializer) MustUInt() uint {
	if val, err := d.UInt(); err != nil {
		panic(err)
	} else {
		return val
	}
}

func (d *Deserializer) MustInt() int {
	if val, err := d.Int(); err != nil {
		panic(err)
	} else {
		return val
	}
}

func (d *Deserializer) MustUInt64() uint64 {
	if val, err := d.UInt64(); err != nil {
		panic(err)
	} else {
		return val
	}
}

func (d *Deserializer) MustInt64() int64 {
	if val, err := d.Int64(); err != nil {
		panic(err)
	} else {
		return val
	}
}

func (d *Deserializer) MustFloat32() float32 {
	if val, err := d.Float32(); err != nil {
		panic(err)
	} else {
		return val
	}
}

func (d *Deserializer) MustFloat64() float64 {
	if val, err := d.Float64(); err != nil {
		panic(err)
	} else {
		return val
	}
}

func (d *Deserializer) MustBool() bool {
	if val, err := d.Bool(); err != nil {
		panic(err)
	} else {
		return val
	}
}

func (d *Deserializer) MustString() string {
	if val, err := d.String(); err != nil {
		panic(err)
	} else {
		return val
	}
}

func (d *Deserializer) MustSerializable(value ISerializable) {
	if err := d.Serializable(value); err != nil {
		panic(err)
	}
}

func (d *Deserializer) MustUInt8Ptr(out *uint8) {
	if err := d.UInt8Ptr(out); err != nil {
		panic(err)
	}
}

func (d *Deserializer) MustBytePtr(out *byte) {
	if err := d.BytePtr(out); err != nil {
		panic(err)
	}
}

func (d *Deserializer) MustInt8Ptr(out *int8) {
	if err := d.Int8Ptr(out); err != nil {
		panic(err)
	}
}

func (d *Deserializer) MustUInt16Ptr(out *uint16) {
	if err := d.UInt16Ptr(out); err != nil {
		panic(err)
	}
}

func (d *Deserializer) MustInt16Ptr(out *int16) {
	if err := d.Int16Ptr(out); err != nil {
		panic(err)
	}
}

func (d *Deserializer) MustUInt32Ptr(out *uint32) {
	if err := d.UInt32Ptr(out); err != nil {
		panic(err)
	}
}

func (d *Deserializer) MustInt32Ptr(out *int32) {
	if err := d.Int32Ptr(out); err != nil {
		panic(err)
	}
}

func (d *Deserializer) MustUIntPtr(out *uint) {
	if err := d.UIntPtr(out); err != nil {
		panic(err)
	}
}

func (d *Deserializer) MustIntPtr(out *int) {
	if err := d.IntPtr(out); err != nil {
		panic(err)
	}
}

func (d *Deserializer) MustUInt64Ptr(out *uint64) {
	if err := d.UInt64Ptr(out); err != nil {
		panic(err)
	}
}

func (d *Deserializer) MustInt64Ptr(out *int64) {
	if err := d.Int64Ptr(out); err != nil {
		panic(err)
	}
}

func (d *Deserializer) MustFloat32Ptr(out *float32) {
	if err := d.Float32Ptr(out); err != nil {
		panic(err)
	}
}

func (d *Deserializer) MustFloat64Ptr(out *float64) {
	if err := d.Float64Ptr(out); err != nil {
		panic(err)
	}
}

func (d *Deserializer) MustBoolPtr(out *bool) {
	if err := d.BoolPtr(out); err != nil {
		panic(err)
	}
}

func (d *Deserializer) MustStringPtr(out *string) {
	if err := d.StringPtr(out); err != nil {
		panic(err)
	}
}

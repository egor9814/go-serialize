package go_serialize

import (
	"errors"
	"io"
	"math"
	"strconv"
)

type Serializer struct {
	out io.Writer
}

func NewSerializer(out io.Writer) *Serializer {
	return &Serializer{out: out}
}

type byteOut struct {
	out []byte
}

func (b *byteOut) Write(p []byte) (n int, err error) {
	b.out = append(b.out, p...)
	return len(p), nil
}

func NewByteArraySerializer(capacity uint) (s *Serializer, buffer *[]byte) {
	out := &byteOut{
		out: make([]byte, 0, capacity),
	}
	buffer = &out.out
	s = NewSerializer(out)
	return
}

func (s *Serializer) Bytes(value []byte) error {
	if n, err := s.out.Write(value); err != nil {
		return err
	} else if n != len(value) {
		return errors.New("cannot write all " + strconv.Itoa(n) + " bytes")
	} else {
		return nil
	}
}

func (s *Serializer) UInt8(value uint8) error {
	return s.Bytes([]byte{value})
}

func (s *Serializer) Byte(value byte) error {
	return s.UInt8(value)
}

func (s *Serializer) Int8(value int8) error {
	return s.UInt8(uint8(value))
}

func (s *Serializer) UInt16(value uint16) error {
	return s.Bytes([]byte{
		byte((value >> 8) & 0xff),
		byte(value & 0xff),
	})
}

func (s *Serializer) Int16(value int16) error {
	return s.UInt16(uint16(value))
}

func (s *Serializer) UInt32(value uint32) error {
	return s.Bytes([]byte{
		byte((value >> 24) & 0xff),
		byte((value >> 16) & 0xff),
		byte((value >> 8) & 0xff),
		byte(value & 0xff),
	})
}

func (s *Serializer) Int32(value int32) error {
	return s.UInt32(uint32(value))
}

func (s *Serializer) UInt(value uint) error {
	return s.UInt32(uint32(value))
}

func (s *Serializer) Int(value int) error {
	return s.Int32(int32(value))
}

func (s *Serializer) UInt64(value uint64) error {
	return s.Bytes([]byte{
		byte((value >> 56) & 0xff),
		byte((value >> 48) & 0xff),
		byte((value >> 40) & 0xff),
		byte((value >> 32) & 0xff),
		byte((value >> 24) & 0xff),
		byte((value >> 16) & 0xff),
		byte((value >> 8) & 0xff),
		byte(value & 0xff),
	})
}

func (s *Serializer) Int64(value int64) error {
	return s.UInt64(uint64(value))
}

func (s *Serializer) Float32(value float32) error {
	return s.UInt32(math.Float32bits(value))
}

func (s *Serializer) Float64(value float64) error {
	return s.UInt64(math.Float64bits(value))
}

func (s *Serializer) Bool(value bool) error {
	if value {
		return s.Byte(1)
	} else {
		return s.Byte(0)
	}
}

func (s *Serializer) String(value string) error {
	data := []byte(value)
	if err := s.Int(len(data)); err != nil {
		return err
	}
	return s.Bytes(data)
}

func (s *Serializer) Serializable(value ISerializable) error {
	return value.Serialize(s)
}

func (s *Serializer) MustBytes(value []byte) {
	if err := s.Bytes(value); err != nil {
		panic(err)
	}
}

func (s *Serializer) MustUInt8(value uint8) {
	if err := s.UInt8(value); err != nil {
		panic(err)
	}
}

func (s *Serializer) MustByte(value byte) {
	if err := s.Byte(value); err != nil {
		panic(err)
	}
}

func (s *Serializer) MustInt8(value int8) {
	if err := s.Int8(value); err != nil {
		panic(err)
	}
}

func (s *Serializer) MustUInt16(value uint16) {
	if err := s.UInt16(value); err != nil {
		panic(err)
	}
}

func (s *Serializer) MustInt16(value int16) {
	if err := s.Int16(value); err != nil {
		panic(err)
	}
}

func (s *Serializer) MustUInt32(value uint32) {
	if err := s.UInt32(value); err != nil {
		panic(err)
	}
}

func (s *Serializer) MustInt32(value int32) {
	if err := s.Int32(value); err != nil {
		panic(err)
	}
}

func (s *Serializer) MustUInt(value uint) {
	if err := s.UInt(value); err != nil {
		panic(err)
	}
}

func (s *Serializer) MustInt(value int) {
	if err := s.Int(value); err != nil {
		panic(err)
	}
}

func (s *Serializer) MustUInt64(value uint64) {
	if err := s.UInt64(value); err != nil {
		panic(err)
	}
}

func (s *Serializer) MustInt64(value int64) {
	if err := s.Int64(value); err != nil {
		panic(err)
	}
}

func (s *Serializer) MustFloat32(value float32) {
	if err := s.Float32(value); err != nil {
		panic(err)
	}
}

func (s *Serializer) MustFloat64(value float64) {
	if err := s.Float64(value); err != nil {
		panic(err)
	}
}

func (s *Serializer) MustBool(value bool) {
	if err := s.Bool(value); err != nil {
		panic(err)
	}
}

func (s *Serializer) MustString(value string) {
	if err := s.String(value); err != nil {
		panic(err)
	}
}

func (s *Serializer) MustSerializable(value ISerializable) {
	if err := s.Serializable(value); err != nil {
		panic(err)
	}
}

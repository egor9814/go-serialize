package go_serialize

type ISerializable interface {
	Serialize(*Serializer) error
	Deserialize(*Deserializer) error
}
